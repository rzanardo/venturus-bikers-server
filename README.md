# Venturus Bikers - Server

## Description

This is a RESTful webservice developed with ExpressJS using Typescript, so make sure you have installed nodets and typescript dependencies. It also uses nodemon to live reload the code after any changes.  
`npm install -g nodemon`  
`npm install -g typescript ts-node`  

(It is a good idea to have these dependencies installed globally)

## Preparing the workspace

After cloning this repository, navigate into its root directory and run `npm install` to download all of its dependencies

## Setting up MongoDB

Make sure you have an instance of MongoDB server running at localhost with the default port 27017.

## Running the server

Simply run the command `npm run dev` to start the server.

### Additional info

Used versions:  
Node: v8.11.2  
NPM: 5.5.1