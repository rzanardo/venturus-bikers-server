import { Request, Response } from "express";
import { BikersController } from "../controllers/bikersController";

export class BikersRoutes {

    public bikersController: BikersController = new BikersController();

    public routes(app): void {
        app.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            next();
        });

        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'Yay! It works!'
                })
            })

        app.route('/biker')
            .get(this.bikersController.getBikers)
            .post(this.bikersController.addNewBiker);

        app.route('/biker/:bikerId')
            .get(this.bikersController.getBikerByID)
            .put(this.bikersController.updateBiker)
            .delete(this.bikersController.deleteBiker);
    }
}