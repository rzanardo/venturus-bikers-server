import * as express from "express";
import * as bodyParser from "body-parser";
import { BikersRoutes } from "./routes/bikersRoutes";
import * as mongoose from "mongoose";

class App {

    public app: express.Application;
    public bikersRoutes: BikersRoutes = new BikersRoutes();
    public mongoUrl: string = 'mongodb://localhost/bikersApp';  

    constructor() {
        this.app = express();
        this.config();
        this.bikersRoutes.routes(this.app);
        this.mongoSetup();
    }

    private config(): void{
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(bodyParser.json());
    }

    private mongoSetup(): void{
        mongoose.connect(this.mongoUrl, {useNewUrlParser:true})
    }
}

export default new App().app;