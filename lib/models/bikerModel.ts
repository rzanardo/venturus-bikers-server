import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const BikerSchema = new Schema({
    fullName: {
        type: String,
        required: 'Enter the full name'
    },
    email: {
        type: String,
        required: 'Enter the email'
    },
    city: {
        type: String            
    },
    groupRide: {
        type: String
    },
    dowMonday: {
        type: Boolean
    },
    dowTuesday: {
        type: Boolean
    },
    dowWednesday: {
        type: Boolean
    },
    dowThursday: {
        type: Boolean
    },
    dowFriday: {
        type: Boolean
    },
    dowSaturday: {
        type: Boolean
    },
    dowSunday: {
        type: Boolean
    },
    registrationDate: {
        type: Date,
        default: Date.now
    }
});