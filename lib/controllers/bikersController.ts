import * as mongoose from 'mongoose';
import { BikerSchema } from '../models/bikerModel';
import { Request, Response } from 'express';

const Biker = mongoose.model('Biker', BikerSchema);

export class BikersController {

    public addNewBiker(req: Request, res: Response) {
        console.log(`POST: addNewBiker -> ${JSON.stringify(req.body)}`)
        let newBiker = new Biker(req.body);

        newBiker.save((err, biker) => {
            if (err) {
                res.status(500).send(err);
            }
            res.json(biker);
        });
    }

    public getBikers(req: Request, res: Response) {
        console.log(`GET: getBikers -> ${JSON.stringify(req.body)}`)

        Biker.find({}, (err, biker) => {
            if (err) {
                res.status(500).send(err);
            }
            res.json(biker);
        });
    }

    public getBikerByID(req: Request, res: Response) {
        Biker.findById(req.params.bikerId, (err, contact) => {
            if (err) {
                res.status(500).send(err);
            }
            res.json(contact);
        });
    }

    public updateBiker(req: Request, res: Response) {
        console.log(`PUT: updateBiker -> ${JSON.stringify(req.body)}`)
        Biker.findOneAndUpdate({ _id: req.params.bikerId }, req.body, { new: true }, (err, biker) => {
            if (err) {
                res.status(500).send(err);
            }
            res.json(biker);
        });
    }

    public deleteBiker(req: Request, res: Response) {
        Biker.remove({ _id: req.params.bikerId }, (err) => {
            if (err) {
                res.status(500).send(err);
            }
            res.json({ message: 'Successfully deleted biker!' });
        });
    }
}