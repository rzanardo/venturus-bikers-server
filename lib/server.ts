import app from "./app";

const PORT = 3000;

app.listen(PORT, () => {
    console.log('Bikers server listening on port ' + PORT);
})